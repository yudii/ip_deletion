package com.shieldsquare.delet.main;

public class IPDelete {

	
	private int total_count ;

	private String updateMsg;
	private String all;
	private String Date;
	private String ip_address;
	
	
	public String getDate() {
		return Date;
	}

	public String getIp_address() {
		return ip_address;
	}

	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getUpdateMsg() {
		return updateMsg;
	}

	public void setUpdateMsg(String updateMsg) {
		this.updateMsg = updateMsg;
	}

	public int getTotal_count() {
		return total_count;
	}
	
	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}
	
	
	
	
}