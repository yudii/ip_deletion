package com.shieldsquare.delet.main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;



public class ReadFile {

	public void read() throws SQLException {

		ArrayList<String> ar = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(
				"/home/yogesh/Desktop/screencap/testing.txt"))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				ar.add(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		DeleteIP_DAO_Impl deleteIPObj=new DeleteIP_DAO_Impl();
		deleteIPObj.deleteIPfromTable(ar);

	}

	
	public static void main(String[] args) throws SQLException {
		ReadFile r=new ReadFile();
		r.read();
	
	}
}
