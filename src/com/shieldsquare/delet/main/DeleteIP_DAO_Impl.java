package com.shieldsquare.delet.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DeleteIP_DAO_Impl {

	
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	int sid = 21;
	String bot_classi;
	String updatemsg;
	String bot;
	final String r1 = "Integrity checks failed";
	final String r2 = "Data Center Bot";
	final String r5 = "Non Browser UserAgent";

	public void deleteIPfromTable(ArrayList<String> ar) throws SQLException {
		for (int i = 0; i <= ar.size() - 1; i++) {
			// System.out.println("IP" + ar.get(i));
			String ipp = ar.get(i);
			try {

				// not using now
				/*
				 * Bol_classification_checks bc=new Bol_classification_checks();
				 * String bcc=bc.bot_Classi(ipp,sid);
				 * System.out.println(bcc+"gsahsagsahgsaghsghasghasgg");
				 */

				con = DBUtils.getMySqlStConnection();

				// getting Bot classification from BlackList_Reason Table
				ps = con.prepareStatement("select bot_classification from Blacklist_Reason where ip_address=? and sid=?");
				ps.setString(1, ipp);
				ps.setInt(2, sid);
				rs = ps.executeQuery();
				while (rs.next()) {
					String bot_classi = rs.getString("bot_classification");
					if (bot_classi != null) {
						bot = bot_classi;
						// System.out.println(bot);
					} else {
						bot = null;
					}
				}

				// getting Data from IP_Analysis
				ps = con.prepareStatement("select dt,total_requests from ip_analysis where ip_address in (select id from ip_details where ip_address=?) and sid=?");
				ps.setString(1, ipp);
				ps.setInt(2, sid);
				rs = ps.executeQuery();

				while (rs.next()) {
					String Date = rs.getString("dt");
					int totalRequests = rs.getInt("total_requests");
					// System.out.println(Date + "-Date and Request - " +
					// totalRequests);

					// bad bots column in Rules_Summary (Integrity checks
					// failed)
					System.out.println(bot + "blacklist value");

					if (bot != null && bot.equalsIgnoreCase(r1)) {
						System.out.println("Bad Botsssss");
						PreparedStatement ps1 = con
								.prepareStatement("update rules_summary set bad_bots = bad_bots - ? where sid=? and dt=?");

						ps1.setInt(1, totalRequests);
						ps1.setInt(2, sid);
						ps1.setString(3, Date);
						ps1.executeUpdate();
						// updatemsg = "Updated in Bad_bots column, value is__"+
						// totalRequests;
						// System.out.println(updatemsg);

					}

					// Datacenter column in Rules_Summary R2(Data_center_bots)

					else if (bot != null && bot.equalsIgnoreCase(r2)) {
						PreparedStatement ps1 = con
								.prepareStatement("update rules_summary set datacenter_bots = datacenter_bots - ? where sid=? and dt=?");
						ps1.setInt(1, totalRequests);
						ps1.setInt(2, sid);
						ps1.setString(3, Date);
						ps1.executeUpdate();

						/*
						 * updatemsg =
						 * "Updated in data_center column value is__" +
						 * totalRequests; System.out.println(updatemsg);
						 */
					}

					// column in Rules_Summary R5(Known_UA_Bots)
					else if (bot != null && bot.equalsIgnoreCase(r5)) {
						System.out.println("Bad UAAAAAAAAAA");
						PreparedStatement ps1 = con
								.prepareStatement("update rules_summary set known_ua_bots = known_ua_bots - ? where sid=? and dt=?");

						ps1.setInt(1, totalRequests);
						ps1.setInt(2, sid);
						ps1.setString(3, Date);
						ps1.executeUpdate();
						updatemsg = "Updated in known_ua_bots column value is__"
								+ totalRequests;
						System.out.println(updatemsg);

					}

				}

				if (bot != null
						&& (bot.equalsIgnoreCase("r1")
								|| bot.equalsIgnoreCase("r2") || bot
									.equalsIgnoreCase("r5"))) {

					ps = con
					// .prepareStatement("delete from ip_analysis output deleted.total_requests as delTreq where ip_address in (select id from ip_details where ip_address=?) and sid=?");
					.prepareStatement("delete from ip_analysis where ip_address in (select id from ip_details where ip_address=?) and sid=?");

					ps.setString(1, ipp);
					ps.setInt(2, sid);
					int rowsDeleted = ps.executeUpdate();
					if (rowsDeleted > 0) {
						System.out.print("IP has been deleted");
					}
				}
				// Clean up
				con.close();
			}

			catch (Exception e) {

				if (con != null) {
					con.rollback();
				}
				e.printStackTrace();
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}
	}
}